// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeGameGameModeBase.generated.h"
#include "Interactable.h"

/**
 * 
 */

UCLASS()
class SNAKEGAME_API ASnakeGameGameModeBase : public AGameModeBase, public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:
	ASnakeGameGameModeBase();

	UPROPERTY(BlueprintCallable)
	float Points;
	
	UFUNCTION(BlueprintCallable)
	void ChangePoints();
};
