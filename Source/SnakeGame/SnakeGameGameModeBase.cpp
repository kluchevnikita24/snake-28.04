// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.


#include "SnakeGameGameModeBase.h"
#include "Interactable.h"
#include "SnakeElementBase.h"

ASnakeGameGameModeBase::ASnakeGameGameModeBase()
{
	Points = 0;
}

void ASnakeGameGameModeBase::ChangePoints()
{
	Points = Points + 1;
}
